Ravleen Bajwa
Qd011561
Software Engineering 
Course work: Number 2
Project Plan


## Contents

1.	Abstract
2.	Introduction
3.	Background
4.	Plans 
   i.  Gantt Chart
   ii. Work Breakdown structure
   iii. CV 

5.	Reflection 
6.	Bibliography/ Sources 


## Abstract

Modules for the course G400 at University of Reading were explored in this document. The duration of this course is 3 years. 4 years if you choose to have a year in industry.
This document will include:
-A Work Breakdown structure: Which displays all the modules at university of Reading that the Computer Science Department has to offer. 
-A Gantt chart: Which will include all the key dates for the modules.
Towards the end of the document, I will be reflecting on what I have learnt from this project.

## Introduction 

University of Reading offers a fantastic opportunity to students who want to dive into the world of technology. There are many wide variety of modules available to select from, starting from year 1- 3 There is also an opportunity for students to join an industrial year, in which case, the course will last for 4 Years
This is a brief preview of what the course requires in terms of credits.:
Year 1:  4 Compulsory modules worth credits. Select optional modules worth 40credits. Overall, 120 credits 
Year 2: 8 Compulsory modules worth 100 credits. Select optional modules worth 20 credits. Overall, 120 credits 
Year 3: *OPTIONAL YEAR IN INDUSTRY OF YOUR CHOICE*

Year 4: 2 Compulsory modules worth 50 credits. Select optional modules worth 70 credits. Overall, 120 credits 
(source : http://www.reading.ac.uk/progspecs/pdf12/UFCOMPB12.pdf )

I have produced a Gannt chart to show more in-depth information on the timeline for the plan of the whole year. This should show more in-depth deadlines and tasks for each module, as well as what percentage they are worth. The Gannt chart will also be for my own personal use to help me stick to target. For example, not submitting assignment on time would deduct 10% of the marks, for every work day that it isn’t submitted. After 50% (5 working days), it is automatically a 0%. (fail)

To pass year 1: Achieve 40% over 120 credits 
To pass year 2: (i) a weighted average of 40% over 120 credits taken at Part 2; (ii) marks of at least 40% in individual modules amounting to not less than 80 credits; and (iii) marks of at least 30% in individual modules amounting to not less than 120 credits. ( source : http://www.reading.ac.uk/progspecs/pdf12/UFCOMPB12.pdf)
To pass year 3: You can either get a job in industry or continue year 3
To pass year 4: “ student must achieve the threshold performance.”

## Background: About myself

The future of technology has never been more exciting. Twenty years ago, no one could possibly imagine where we are now and the thought of where we could be in another twenty years. To be able to be part of that development would be incredible. The power of technology poses an unprecedented challenge from Blockchains being used for cryptocurrency to AI and robotics revolutionising businesses. It is my ambition to immerse myself in this emerging field. I aim to be part of the next generation of computer scientists that will pave the way for innovation and to push boundaries to new heights.
To achieve insight on where technology stands currently, I’ve attended a competitive experience placement at Cisco headquarters. This gave me the chance to learn about the role of a technology company and gave me an insight into some of the projects they were undertaking. Whilst there, I was able to engage in discussions related to Artificial Intelligence and its impact on society; it allowed me to gain a deeper appreciation of technology and its real-world applications. I was also able to take part in a Dragons Den team challenge which involved creating a virtual assistant for geriatric users. Our creative ideas and ability to think outside the box meant we were awarded first place at Cisco.
I have taken part in a work experience placement at Fujitsu to learn more about cyber security. Engaging with mentors in this field has motivated me to undertake self-learning courses on Immersive labs which has taught me about the fundamentals on cyber security such as: introduction to cyber security, ethics, terminology & cryptocurrency.
My passion for this field and my background of studying A Level Business prompted me to pick a topic that was a combination of both my interests. This project has allowed me to develop my analytical and independent ability; skills which will help me during my degree.
Studying BTEC computing has given me a thorough grounding of the basics, through online tutorials from W3 schools, I have learnt how to build a website using HTML and CSS. My part time job at Explore Learning as a Maths tutor to young people has allowed me to develop my communication skills and the ability to convey complex ideas in a simple and clear way. I have learnt how to communicate effectively within a wide range of ages and ability levels; skills which are needed in a collaborative field like computer science.
As part of extracurricular activities, I have been an active member of the community at College. I have held positions on the student board and the equalities council. I have been involved in organising the college prom; which required planning, preparation and teamwork.
The equalities council focuses on supporting students who are struggling academically or emotionally, I have found this a really rewarding experience. I believe Computer Science is a developing field both in terms of research and technology but also for females. It has been seen very much as a male dominated world but this is rapidly changing, and I am excited to be able to be part of that change.
Learning in depth about computer science at university will allow me to root my technical knowledge on computer systems, helping me build towards my genuine long-term goal of being a data analyst.


## Plans
This Gannt Chart contains activities of modules, since I wasn’t able to save it on my device, here is a breakdown of the chart.
            






Work Breakdown Structure: Here are different screenshots as I was unable to save the whole structure.
 
 


## CV
 
## Reflection

Upon attempting the task, to make my Work Breakdown structure and Gantt chart, I came across a few software applications to understand which one best suited by budget as a student and time. For my Gantt chart, I came across a website called Tom’s Planner  (https://www.tomsplanner.com/). I have used a Gantt chart before at my college. I used Microsoft Projects to help with this. I was not able to use Microsoft Projects because my laptop is not compatible with the software. Hence why I found Tom’s planner useful. It was especially helpful that the work was saved online, rather than on my device (Microsoft Projects), so that it was easily accessible, regardless of where I was working on it from. I especially found it useful that the website was easy to use. The template for the Gantt chart was laid out very simply, only focusing on things that were important things such as activities, resources, start and end of deadline. Which is why I believed this was the best software/website to use. While making my Gannt chart, I was able to visualise a brief outline of how my course is going to be, helping to understand what assignments/tests are coming next. This will help me stay on target. When producing the Gannt chart, the resources used to for the date of the deadlines were out of date and some data was missing. Deadlines were shown for the year 2018/2019. This meant that there were a lot of assumptions made for the dates for the years 2019-2023. Along with this, starting dates and due dates were also not included. This means that I was able to make a guess on the duration of the tasks, depending on what percentage of the module they were.
Secondly, The Work Breakdown diagram is a visual representation of the modules available in the course.  I chose Draw.io (https://www.draw.io/ ) to make a Work Breakdown structure as again, this website allowed me to save my document into the cloud which meant, I was able to access it from the computer lab or the ease of my home. This saved a lot of time of transferring files. I especially liked Draw.io due to its drag and drop feature. In software such as word it is difficult to drag and drop, and you are expected to make the shape of the diagram from scratch. When attempting this task, I was unsure of what the Work Breakdown structure was, I used a Wikipedia link to help get further understanding on this ( https://en.wikipedia.org/wiki/Work_breakdown_structure  ) Producing a Work Breakdown structure has influenced me to think more about what possible modules I might be choosing for the years ahead to help achieve my goals.
It should also consider the change in approach between school/college and university in terms of your personal ownership of learning, and your approaches to handling the uncertainty involved.
Overall, I believe that there was a major transition from college to university. In college, the information is handed to you. However, in university a variety of information is explored. The information is also gone through quickly. Unlike college, what you learn in one lecture, may not be practiced in the next. That is up to me as a student to go home and practice. This requires a lot of dedication and self-motivation.  Speaking of which, attendance isn’t monitored in university (apart for tutorials), which again depends on self-motivation of the student. Parents will not be emailed regarding the attendance, but the student will face consequences such as low grades, for not catching behind. There is a lot of work load in university, if I am not organised, I will not be able to have a balanced social and work life. Which is why, staying organised is truly important, along with catching up with missed work.

## Bibliography :

Software:
Gantt Chart: https://www.tomsplanner.com/
Work Breakdown structure: https://www.draw.io.com/ 
Module Information: https://www.reading.ac.uk/modules/module.aspx?sacyr=1617&school=MPS
Course Information: http://www.reading.ac.uk/progspecs/pdf12/UFCOMPB12.pdf
Work Breakdown Structure: https://en.wikipedia.org/wiki/Work_breakdown_structure   




